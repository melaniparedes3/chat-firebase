import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import firebase from 'firebase'

Vue.config.productionTip = false

const firebaseConfig = {
  apiKey: "AIzaSyDbpV_3INHUNtJjcoYpypxncUAL2IanEvI",
  authDomain: "chat-project-46c54.firebaseapp.com",
  projectId: "chat-project-46c54",
  storageBucket: "chat-project-46c54.appspot.com",
  messagingSenderId: "166953868533",
  appId: "1:166953868533:web:3e66ba92b24df595ed79bc",
  measurementId: "G-ZG7L29FF90",

};

firebase.initializeApp(firebaseConfig)

firebase.auth().onAuthStateChanged(() =>  new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app'))
